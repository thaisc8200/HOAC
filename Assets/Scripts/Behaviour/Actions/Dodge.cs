using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class Dodge : Action
{

    private AgentMovement _AgentMovement;
    private Animator _Animator;
    private Controller _Controlador;
    private Vector3 _Target;

    public override void OnStart()
	{
        _AgentMovement = GetComponent<AgentMovement>();
        _Animator = GetComponent<Animator>();
        _Controlador = GetComponent<Controller>();
    }

    public override TaskStatus OnUpdate()
    {
        if (Vector3.SqrMagnitude(transform.position - _Target) < 1f)
        {
            return TaskStatus.Success;
        }
        //Move to y activar anim.
        return TaskStatus.Running;
	}
}