using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

namespace G1.IA
{
    public class Flip : Action
    {
        private GameObject _Player;
        private Animator _Animator;

        public float speed = 10f;

        public override void OnStart()
        {
            _Animator = GetComponent<Animator>();
            _Player = GameObject.FindGameObjectWithTag("Player");
            _Animator.SetBool("Turn", true);
        }

        public override TaskStatus OnUpdate()
        {
            if (_Player == null)
            {
                _Player = GameObject.FindGameObjectWithTag("Player");
            }
            FlipMethod();
            return TaskStatus.Success;
        }

        void FlipMethod()
        {
            Vector3 direction = _Player.transform.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speed * Time.deltaTime);
            _Animator.SetBool("Turn", false);
        }
    }
}