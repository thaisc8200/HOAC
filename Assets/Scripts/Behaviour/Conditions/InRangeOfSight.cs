using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class InRangeOfSight : Conditional
{
    private GameObject _Player;

    public SharedTransform Target;
    public float FieldOfViewAngle = 20f;

    public override void OnAwake()
    {
        _Player = GameObject.FindGameObjectWithTag("Player");
    }

    public override TaskStatus OnUpdate()
    {
        if (_Player == null)
        {
            _Player = GameObject.FindGameObjectWithTag("Player");
            Target.SetValue(_Player.transform);
            if (WithinSight())
            {
                return TaskStatus.Success;
            }
            return TaskStatus.Failure;
        }
        else
        {
            if (WithinSight())
            {
                return TaskStatus.Success;
            }
            return TaskStatus.Failure;
        }
    }

    public bool WithinSight()
    {
        Vector3 direction = _Player.transform.position - transform.position;
        float angle = Vector3.Angle(direction, transform.forward);
        if (angle < FieldOfViewAngle)
        {
            return true;
        } else
        {
            return false;
        }
    }
}