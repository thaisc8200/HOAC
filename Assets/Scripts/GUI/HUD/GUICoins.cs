﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GUICoins : MonoBehaviour
{
    public TextMeshProUGUI CurrentCoins;
    public TextMeshProUGUI AddedCoins;

    private CanvasGroup m_CanvasGroup;
    
    private Coroutine m_ShowCoroutine;
    
    private int m_TotalCoins = 0;
    private int m_Acumulate = 0;
    
    private const float SHOW_TIME = 2f;

    private void Awake()
    {
        m_CanvasGroup = GetComponent<CanvasGroup>();
    }

    private void Start()
    {
        m_CanvasGroup.alpha = 0f;
    }

    public void UpdateCoins()
    {
        CurrentCoins.text = m_TotalCoins.ToString();
        
        if (m_ShowCoroutine != null)
        {
            StopCoroutine(m_ShowCoroutine);
        }
        
        m_ShowCoroutine = StartCoroutine(ShowCoins());
    }

    public void UpdateDiffCoins(int coins = 0)
    {
        m_Acumulate += coins;
        if (coins > 0) AddedCoins.text = "+" + m_Acumulate;
        else AddedCoins.text = "-" + Mathf.Abs(m_Acumulate);
    }

    private IEnumerator ShowCoins()
    {
        m_CanvasGroup.alpha = 1f;
        yield return new WaitForSeconds(SHOW_TIME);
        m_Acumulate = 0;
        m_TotalCoins = MatchManager.Instance.CurrentCoins;
        AddedCoins.text = "";
        CurrentCoins.text = m_TotalCoins.ToString();
        yield return new WaitForSeconds(1f);
        m_CanvasGroup.alpha = 0f;
    }
}
