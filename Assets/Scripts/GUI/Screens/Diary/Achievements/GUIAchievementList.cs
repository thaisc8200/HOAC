﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIAchievementList : MonoBehaviour
{
    public RectTransform Layout;
    public GameObject AchievementPrefab;
    public int Page;
    public int MaxAchievementsPerPage = 6;

    public void DisplayContent(List<AchievementStatus> achievements)
    {
        int iterator = 0;
        switch (Page)
        {
            case 1:
                iterator = 0;
                break;
            case 2:
                iterator += 5;
                break;
            case 3:
                iterator += 11;
                break;
            case 4:
                iterator += 16;
                break;
            case 5:
                iterator += 21;
                break;
            case 6:
                iterator += 26;
                break;
            default:
                iterator = 0;
                break;
        }

        int max = iterator + MaxAchievementsPerPage;
        for (int i = iterator; i < achievements.Count && i < max; i++)
        {
            var achievement = achievements[i];
            GUIAchievementSlot slot = Instantiate(AchievementPrefab, Layout).GetComponent<GUIAchievementSlot>();
            slot.Init(achievement);
        }
    }

    public void EraseContent()
    {
        foreach (RectTransform child in Layout)
        {
            Destroy(child.gameObject);
        }
        Layout.DetachChildren();
    }
}
