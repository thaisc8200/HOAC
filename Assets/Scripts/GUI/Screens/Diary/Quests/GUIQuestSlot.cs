﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GUIQuestSlot : MonoBehaviour
{
    public TextMeshProUGUI Title;
    public TextMeshProUGUI Description;

    [Header("Layouts")]
    public RectTransform ProgressLayout;
    public RectTransform RewardsLayout;
    
    [Header("Prefabs")]
    public GameObject ProgressPrefab;
    public GameObject RewardPrefab;
    
    [Header("Rewards")]
    public GUIQuestReward XpReward;
    public GUIQuestReward MoneyReward;
    
    public QuestStatus Quest { get; set; }

    public void Init(QuestStatus quest)
    {
        Quest = quest;

        Title.text = LocalizationManager.Instance["QUEST", quest.Quest.QuestNameTextKey];
        Description.text = LocalizationManager.Instance["QUEST", quest.Quest.QuestShortDescriptionTextKey];
        
        DisplayProgress();
        DisplayRewards();
    }

    private void DisplayProgress()
    {
        foreach (var goal in Quest.Progression.Goals)
        {
            GUIQuestProgress progress = Instantiate(ProgressPrefab, ProgressLayout).GetComponent<GUIQuestProgress>();
            CheckState(progress);
            progress.Init(goal);
        }
    }

    private void DisplayRewards()
    {
        XpReward.SetAmount(Quest.Quest.QuestReward.ExperienceReward);
        MoneyReward.SetAmount(Quest.Quest.QuestReward.MoneyReward);
        foreach (var reward in Quest.Quest.QuestReward.ItemRewards)
        {
            GUIQuestReward item = Instantiate(RewardPrefab, RewardsLayout).GetComponent<GUIQuestReward>();
            item.InitItem(reward);
        }
    }

    private void CheckState(GUIQuestProgress progress)
    {
        if (Quest.Progression.CurrentState == QuestProgression.State.Completed)
        {
            progress.SliderFill.color = new Color(1f,0.64f,0f,1f);
        } 
        else if (Quest.Progression.CurrentState == QuestProgression.State.Finished)
        {
            Title.fontStyle = FontStyles.Strikethrough;
            Description.fontStyle = FontStyles.Strikethrough;
            progress.SliderFill.color = Color.green;
        }
        else
        {
            Title.fontStyle = FontStyles.Bold;
            Description.fontStyle = FontStyles.Normal;
        }
    }
}
