﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class GUIAskQuest : MonoBehaviour
{
   public GameObject FirstGameObject;
   public TextMeshProUGUI NpcText;
   
   public Transform Opener { get; set; }

   private NPCQuest m_NpcQuest;
   
   private CanvasGroup m_MenuCanvasGroup;
   private GameObject m_PreviousSelected;
   
   private void Awake()
   {
      m_MenuCanvasGroup = GetComponent<CanvasGroup>();
   }

   public void ShowAskQuest()
   {
      UIHUD.Instance.EnableHUD(false);
      if (m_PreviousSelected != null)
      {
         EventSystem.current.SetSelectedGameObject(m_PreviousSelected);
      }
      else
      {
         EventSystem.current.SetSelectedGameObject(FirstGameObject);
      }

      m_MenuCanvasGroup.interactable = true;
   }

   public void OpenAskQuest(Transform opener, NPCQuest npcQuest)
   {
      Opener = opener;
      m_NpcQuest = npcQuest;
      ShowAskQuest();
      LoadText();
   }
   
   public void CloseAskQuest()
   {
      UIHUD.Instance.EnableHUD(true);
      m_NpcQuest.CloseAskQuest(Opener);

      Opener = null;
      m_NpcQuest = null;

      GUIManager.Instance.CloseAskQuestScreen();
   }
   
   public void HideMenuNoAnim()
   {
      m_MenuCanvasGroup.alpha = 0;
      m_MenuCanvasGroup.interactable = false;
   }
   
   public void ShowMenuNoAnim()
   {
      UIHUD.Instance.EnableHUD(false);
      m_MenuCanvasGroup.alpha = 1;
      m_MenuCanvasGroup.interactable = true;
      if (m_PreviousSelected != null)
      {
         EventSystem.current.SetSelectedGameObject(m_PreviousSelected);
      }
      else
      {
         EventSystem.current.SetSelectedGameObject(FirstGameObject);
      }
   }

   public void OpenShowQuest()
   {
      m_PreviousSelected = EventSystem.current.currentSelectedGameObject;
      GUIManager.Instance.OpenShowQuestScreen(m_NpcQuest.CurrentQuest, Opener, m_NpcQuest);
   }
   
   private void LoadText()
   {
      NpcText.text = LocalizationManager.Instance["QUEST", m_NpcQuest.MessageText];
   }
}
