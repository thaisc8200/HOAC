﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GUIShowQuest : MonoBehaviour
{
    public GameObject FirstGameObject;
    
    [Header("UI Elements")]
    public TextMeshProUGUI Title;
    public TextMeshProUGUI Description;
    public Image NPCPortrait;
    
    [Header("Rewards")]
    public RectTransform RewardsLayout;
    public GameObject ItemRewardPrefab;
    public GUIQuestReward XPReward;
    public GUIQuestReward MoneyReward;
    
    public Quest Quest { get; set; }
    public Transform Opener { get; set; }
    public NPCQuest NPCQuest { get; set; }

    public void OpenShowQuest(Quest quest, Transform opener, NPCQuest npc)
    {
        Quest = quest;
        Opener = opener;
        NPCQuest = npc;

        Title.text = LocalizationManager.Instance["QUEST", Quest.QuestNameTextKey];
        Description.text = LocalizationManager.Instance["QUEST", Quest.QuestDescriptionTextKey];
        
        NPCPortrait.sprite = NPCQuest.NPC.DialoguePortrait;
        
        XPReward.SetAmount(Quest.QuestReward.ExperienceReward);
        MoneyReward.SetAmount(Quest.QuestReward.MoneyReward);
        
        foreach (var reward in Quest.QuestReward.ItemRewards)
        {
            GUIQuestReward item = Instantiate(ItemRewardPrefab, RewardsLayout).GetComponent<GUIQuestReward>();
            item.InitItem(reward);
        }
        
        EventSystem.current.SetSelectedGameObject(FirstGameObject);
    }

    public void AcceptQuest()
    {
        QuestManager.Instance.AcceptQuest(Quest);
        GUIManager.Instance.CloseShowQuestScreen();
    }

    public void RejectQuest()
    {
        GUIManager.Instance.CloseShowQuestScreen();
    }
}
