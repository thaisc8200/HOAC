﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GUIDialogue : Singleton<GUIDialogue>
{
    public GameObject DialogueBox;
    public TextMeshProUGUI TextBox;

    public DialoguePortrait Agent;
    public DialoguePortrait Conversator;

    public DialogueBox AgentBox;
    public DialogueBox ConversatorBox;

    public GameObject SkipButton;
    
    private CanvasGroup m_CanvasGroup;
    private DialogueGraph m_CurrentDialogue;

    protected override void Awake()
    {
        base.Awake();

        m_CanvasGroup = GetComponent<CanvasGroup>();
    }

    public void Init(DialogueGraph graph, Transform opener)
    {
        if (m_CurrentDialogue != null && m_CurrentDialogue == graph) return;
        
        StartCoroutine(ShowSkipImage());

        m_CanvasGroup.alpha = 1;
        m_CanvasGroup.interactable = true;
        m_CurrentDialogue = graph;

        switch (graph.Agent.Speaker)
        {
            case DialogueSpeaker.DialogueSpeakerType.None:
                AgentBox.SetSpeakerName(string.Empty);
                Agent.Init(null);
                break;
            case DialogueSpeaker.DialogueSpeakerType.Player:
                DialogueAgentData data = opener.gameObject.GetComponent<DialogueAgent>().Data;
                //AgentBox.SetSpeakerName(LocalizationManager.Instance["DIALOGUE", data.Name]);
                AgentBox.SetSpeakerName(GameManager.Instance.PlayerName);
                Agent.Init(data.Portrait);
                break;
            case DialogueSpeaker.DialogueSpeakerType.ExternalAgent:
                AgentBox.SetSpeakerName(LocalizationManager.Instance["DIALOGUE", graph.Agent.Data.Name]);
                Agent.Init(graph.Agent.Data.Portrait);
                break;
        }

        switch (graph.Conversator.Speaker)
        {
            case DialogueSpeaker.DialogueSpeakerType.None:
                ConversatorBox.SetSpeakerName(string.Empty);
                Conversator.Init(null);
                break;
            case DialogueSpeaker.DialogueSpeakerType.Player:
                DialogueAgentData data = opener.gameObject.GetComponent<DialogueAgent>().Data;
                //ConversatorBox.SetSpeakerName(LocalizationManager.Instance["DIALOGUE", data.Name]);
                ConversatorBox.SetSpeakerName(GameManager.Instance.PlayerName);
                Conversator.Init(data.Portrait);
                break;
            case DialogueSpeaker.DialogueSpeakerType.ExternalAgent:
                ConversatorBox.SetSpeakerName(LocalizationManager.Instance["DIALOGUE", graph.Conversator.Data.Name]);
                Conversator.Init(graph.Conversator.Data.Portrait);
                break;
        }
    }

    public void Exit()
    {
        StopTyping();
        
        SkipButton.SetActive(false);
        
        m_CanvasGroup.alpha = 0;
        m_CanvasGroup.interactable = false;
        m_CurrentDialogue = null;

        AgentBox.Hide();
        ConversatorBox.Hide();
    }

    public void StopTyping()
    {
        StopAllCoroutines();
        AgentBox.StopAllCoroutines();
        ConversatorBox.StopAllCoroutines();
    }
    
    public void ShowChat(Chat chat)
    {
        StartCoroutine(ShowChatCo(chat));
    }
    
    private IEnumerator ShowChatCo(Chat chat)
    {
        EntryTransition(chat);

        switch (chat.Speaker)
        {
            case Chat.SpeakerType.Left:
                if (chat.Instant)
                {
                    AgentBox.InstantType(LocalizationManager.Instance["DIALOGUE", chat.Text]);
                }
                else
                {
                    AgentBox.Type(LocalizationManager.Instance["DIALOGUE", chat.Text]);
                }
                break;
            case Chat.SpeakerType.Right:
                if (chat.Instant)
                {
                    AgentBox.InstantType(LocalizationManager.Instance["DIALOGUE", chat.Text]);
                }
                else
                {
                    ConversatorBox.Type(LocalizationManager.Instance["DIALOGUE", chat.Text]);
                }

                break;
        }

        yield return new WaitWhile(() => DialogueManager.Instance.State.State == DialogueInfo.DialogueState.Typing);

        ChatEnd(chat);
    }
    
    private void EntryTransition(Chat chat)
    {
        switch (chat.Speaker)
        {
            case Chat.SpeakerType.Left:
                Agent.Show(DialoguePortrait.Mode.White);
                Conversator.Show(DialoguePortrait.Mode.Black);

                AgentBox.Show();
                ConversatorBox.Hide();
                break;
            case Chat.SpeakerType.Right:
                Agent.Show(DialoguePortrait.Mode.Black);
                Conversator.Show(DialoguePortrait.Mode.White);

                AgentBox.Hide();
                ConversatorBox.Show();
                break;
        }

        AgentBox.Clear();
    }
    
    private void ChatEnd(Chat chat)
    {
        switch (chat.Speaker)
        {
            case Chat.SpeakerType.Left:
                if (chat.Answers.Count > 0)
                {
                    AgentBox.AddOptions(chat.Answers);
                }
                else
                {
                    AgentBox.ShowNextIcon();
                }

                break;
            case Chat.SpeakerType.Right:
                if (chat.Answers.Count > 0)
                {
                    ConversatorBox.AddOptions(chat.Answers);
                }
                else
                {
                    ConversatorBox.ShowNextIcon();
                }
                break;
        }

        EventManager.TriggerEvent(new DialogueEvent(DialogueEvent.EventType.WaitForResponse, DialogueManager.Instance.State.Dialogue, DialogueManager.Instance.Opener));
        DialogueManager.Instance.State.State = DialogueInfo.DialogueState.WaitForResponse;
    }
    
    private IEnumerator ShowSkipImage()
    {
        yield return new WaitForSeconds(1f);
        SkipButton.SetActive(true);
    }
    
    public void SkipDialogue()
    {
        DialogueManager.Instance.SkipDialogue();
    }
    
    public void OpenDialogue()
    {
        m_CanvasGroup.alpha = 1;
        m_CanvasGroup.interactable = true;
    }

    public void CloseDialogue()
    {
        m_CanvasGroup.alpha = 0;
        m_CanvasGroup.interactable = false;
    }
}
