﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class GUIBooleanModal : MonoBehaviour
{
    [Header("Modal Components")]
    public TextMeshProUGUI Question;
    public TextMeshProUGUI Yes;
    public TextMeshProUGUI No;

    public CancelableButton YesButton;
    public CancelableButton NoButton;

    public virtual void Init(string question, string yes, string no, UnityAction yesEvent, UnityAction noEvent, params object[] additional)
    {
        Question.text = question;
        Yes.text = yes;
        No.text = no;

        YesButton.onClick.AddListener(yesEvent);
        YesButton.onCancel.AddListener(noEvent);

        NoButton.onClick.AddListener(noEvent);
        NoButton.onCancel.AddListener(noEvent);

        EventSystem.current.SetSelectedGameObject(YesButton.gameObject);
    }

    public void CloseModal()
    {
        Destroy(gameObject);
    }
}
