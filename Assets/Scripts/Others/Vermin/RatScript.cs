﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatScript : MonoBehaviour
{

    public GameObject[] Points;

    private Animator m_Animator;
    private int m_Point;
    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Point = 0;
    }

    // Update is called once per frame
    void Update()
    {
        SeekForFood();
       
    }

    private void SeekForFood()
    {
        if (m_Point == Points.Length)
        {
            m_Point = 0;
        }
        else
        {
            Vector3 distance = Points[m_Point].transform.position - transform.position;
            float Magnitude = Vector3.SqrMagnitude(distance);
            if (Magnitude < 6f)
            {
                m_Point++;
            }
            else
            {
                Vector3 pos = new Vector3(Points[m_Point].transform.position.x, transform.position.y,Points[m_Point].transform.position.z);
                transform.LookAt(pos);
                transform.position = Vector3.MoveTowards(transform.position, pos, (1f * Time.deltaTime));
            }
        }
    }
}
