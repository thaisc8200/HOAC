﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

[NodeTint("#FF0845")]
public class DialogueDecision : DialogueBaseNode
{
    public int FirstResponse;

    [Serializable]
    public enum Type
    {
        EQUALS, GREATER, LESS
    }

    public List<int> number;

    public List<Type> Types;
    
    [Output(backingValue = ShowBackingValue.Never)] public DialogueBaseNode DefaultDialogue;

    public override void Skip()
    {
        SetAnswered((graph as DialogueGraph).TimesTalked);
    }

    public override void Trigger()
    {
        SetAnswered((graph as DialogueGraph).TimesTalked);
        DialogueManager.Instance.ExecuteNextNode(0);
    }

    public void SetAnswered(int number)
    {
        FirstResponse = number;
    }
    
    public override DialogueBaseNode GetNextNode(int index)
    {
        return GetRequiredNode();
    }

    private DialogueBaseNode GetRequiredNode()
    {
        NodePort exitPort = null;
        try
        {
            for (int i = 0; i < Types.Count; i++)
            {
                switch (Types[i])
                {
                    case Type.GREATER:
                        if (number[i] < FirstResponse)
                        {
                            exitPort = GetOutputPort("Dialogue " + i);
                        }

                        break;
                    case Type.LESS:
                        if (number[i] > FirstResponse)
                        {
                            exitPort = GetOutputPort("Dialogue " + i);
                        }

                        break;
                    case Type.EQUALS:
                        if (number[i] == FirstResponse)
                        {
                            exitPort = GetOutputPort("Dialogue " + i);
                        }

                        break;
                }

                if (exitPort != null) break;
            }

            if (!exitPort.IsConnected)
            {
                exitPort = GetOutputPort("DefaultDialogue");
            }
        }
        catch
        {
            exitPort = GetOutputPort("DefaultDialogue");
        }
        return exitPort.GetConnections()[0].node as DialogueBaseNode;
    }
}
