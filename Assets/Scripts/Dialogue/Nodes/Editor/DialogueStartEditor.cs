﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

[CustomNodeEditor(typeof(DialogueStart))]
public class DialogueStartEditor : NodeEditor
{
    public override void OnBodyGUI()
    {
        serializedObject.Update();

        DialogueStart node = target as DialogueStart;

        GUILayout.BeginHorizontal();
        NodeEditorGUILayout.PortField(GUIContent.none, target.GetOutputPort("output"), GUILayout.MinWidth(0));
        GUILayout.EndHorizontal();

        EditorGUILayout.Space();

        serializedObject.ApplyModifiedProperties();
    }

    public override int GetWidth()
    {
        return 150;
    }
}
