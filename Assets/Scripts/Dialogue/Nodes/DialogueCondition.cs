﻿using System;
using UnityEngine;
using XNode;

[NodeTint("#F7B103")]
public class DialogueCondition : DialogueBaseNode
{
    [Output] public DialogueBaseNode pass;
    [Output] public DialogueBaseNode fail;

    public bool Success;

    public override void Trigger() {
        DialogueManager.Instance.ExecuteNextNode(0);
    }

    public override DialogueBaseNode GetNextNode(int index)
    {
        return GetRequiredNode();
    }

    private DialogueBaseNode GetRequiredNode()
    {
        NodePort exitPort = null;
        if (Success) exitPort = GetOutputPort("pass");
        else exitPort = GetOutputPort("fail");
        return exitPort.GetConnections()[0].node as DialogueBaseNode;
    }
}


