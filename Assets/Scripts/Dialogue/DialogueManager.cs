﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct DialogueEvent
{
    public enum EventType
    {
        Start, End, WaitForResponse
    }

    public EventType Type;
    public DialogueGraph Graph;
    public Transform Opener;

    public DialogueEvent(EventType type, DialogueGraph graph, Transform opener)
    {
        Type = type;
        Graph = graph;
        Opener = opener;
    }
}

public class DialogueInfo
{
    public enum DialogueState
    {
        Transition, Typing, TypingEnd, WaitForResponse
    }
    
    public DialogueGraph Dialogue;
    public DialogueBaseNode Node;
    public DialogueState State;
    
    public DialogueInfo(DialogueGraph dialogue, DialogueBaseNode node)
    {
        Dialogue = dialogue;
        Node = node;
        State = DialogueState.Transition;
    }
}

public class DialogueManager : Singleton<DialogueManager>
{
    public DialogueInfo State { get; set; }
    public Transform Opener { get; set; }

    public void StartDialogue(DialogueGraph dialogue, Transform opener, Transform trigger, int timesTalked)
    {
        EventManager.TriggerEvent(new DialogueEvent(DialogueEvent.EventType.Start, dialogue, Opener));

        Opener = opener;
        
        dialogue.Initialize(trigger,opener,timesTalked);
        
        State = new DialogueInfo(dialogue, dialogue.GetRootNode());

        GUIDialogue.Instance.Init(State.Dialogue, Opener);

        ExecuteNode();
    }

    public void CloseDialogue()
    {
        GUIDialogue.Instance.Exit();
        
        EventManager.TriggerEvent(new DialogueEvent(DialogueEvent.EventType.End, State.Dialogue, Opener));

        State = null;
        Opener = null;
    }

    public void ShowChat(Chat chat)
    {
        GUIDialogue.Instance.ShowChat(chat);
    }

    public void ExecuteNode()
    {
        State.State = DialogueInfo.DialogueState.Transition;
        State.Node.Trigger();
    }

    public void SkipNode()
    {
        State.State = DialogueInfo.DialogueState.Transition;
        if (State.Node is Chat) GUIDialogue.Instance.StopTyping();
        
        State.Node.Skip();
        
        if(State != null) State.Node.Trigger();
    }
    
    public void LoadNextNode(int index)
    {
        State.Node = State.Node.GetNextNode(index);
    }
    
    public void ExecuteNextNode(int index = 0)
    {
        LoadNextNode(index);
        ExecuteNode();
    }
    
    public void SkipDialogue()
    {
        if (State == null)
        {
            return;
        }

        if (State.Node is Chat && (State.Node as Chat).Answers.Count > 0)
        {
            return;
        }

        if (State.Node.GetNextNode(0) == null)
        {
            return;
        }

        LoadNextNode(0);
        SkipNode();

        SkipDialogue();
    }
}
