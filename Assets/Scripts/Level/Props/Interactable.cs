﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public struct InteractionEvent
{
    public enum Type { Start, End }

    public Type InteractionType;

    public InteractionEvent(Type type)
    {
        InteractionType = type;
    }
}

[Serializable]
public class InteractionCallback : UnityEvent<Character>
{

}

[RequireComponent(typeof(Collider))]
public class Interactable : MonoBehaviour
{
    public enum Activation
    {
        Manual, Auto, OnlyFirstAuto
    }

    [Header("NPC Data")]
    public NPC NPC;
    
    [Header("Activation Conditions")]
    public bool CanOnlyActivateIfGrounded = false;
    public Activation ActivationType;

    [Header("Number of Activations")]
    public bool UnlimitedActivations = true;
    public int MaxNumberOfActivations = 0;

    [Header("Visual Prompt")]
    public GameObject PromptGameobjectOverride;
    public bool AlwaysShowPrompt = false;
    public bool ShowPromptWhenColliding = true;
    public bool HidePromptAfterUse = true;
    public Vector3 PromptRelativePosition = Vector3.zero;
    
    [Header("Events")]
    public InteractionCallback OnInteractAction;
    public InteractionCallback OnInteractExit;

    protected int m_NumberOfActivationsLeft;
    protected int m_NumberOfActivations;
    protected bool m_PromptHiddenForever;
    protected GameObject m_Prompt;
    protected bool m_Interacting;
    protected Coroutine m_PromptCo;
    protected bool m_LastPromptState;
    private Animator m_Animator;
    
    public Character Character { get; set; }
    public Character InteractionAgent { get; protected set; }
    public int NumberOfActivations { get { return m_NumberOfActivations;  } }
    public int NumberOfActivationLeft { get { return m_NumberOfActivationsLeft;  } }
    
    protected virtual bool Available
    {
        get
        {
            if (!CheckNumberOfUses() || m_Interacting || CharacterInteraction.InteractionHappening || LoadingSceneManager.Instance.Transitioning)
            {
                return false;
            }

            return true;
        }
    }

    protected virtual void Awake()
    {
        Character = null;
        
        m_Animator = GetComponent<Animator>();
        
        if (PromptGameobjectOverride != null)
        {
            m_Prompt = (GameObject)Instantiate(PromptGameobjectOverride, transform);
            m_Prompt.transform.rotation = Quaternion.identity;
        }
    }
    
    protected virtual void Start()
    {
        m_NumberOfActivationsLeft = MaxNumberOfActivations;
        if (AlwaysShowPrompt)
        {
            ShowPrompt();
        }
    }

    private void OnDisable()
    {
        if (Character != null)
        {
            Character.gameObject.GetComponent<CharacterInteraction>().ExitInteractionZone();
        }
    }

    public void SetTalkBool(bool talking)
    {
        m_Animator.SetBool("Talk", talking);
    }

    protected virtual void EnterZone(Character character)
    {
        if(Character == character) return;

        Character = character;
        character.gameObject.GetComponent<CharacterInteraction>().EnterInteractionZone(this);
        
        switch (ActivationType)
        {
            case Activation.Auto:
                InteractionRequest(character);
                break;
            case Activation.OnlyFirstAuto:
                if (NumberOfActivations == 0)
                {
                    InteractionRequest(character);
                }
                else
                {
                    if (CanShowPrompt(character) && ShowPromptWhenColliding)
                    {
                        ShowPrompt();
                    }
                }
                break;
            case Activation.Manual:
                if (CanShowPrompt(character) && ShowPromptWhenColliding)
                {
                    ShowPrompt();
                }
                break;
        }
    }

    protected void ExitZone(Character character)
    {
        if(Character != character) return;
        Character = null;
        character.gameObject.GetComponent<CharacterInteraction>().ExitInteractionZone();
        
        if ((m_Prompt != null) && !AlwaysShowPrompt && Character == null)
        {
            if (gameObject.activeInHierarchy)
            {
                m_PromptCo = StartCoroutine(HidePrompt());
                m_LastPromptState = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        CheckEnterTrigger(other);
    }

    private void OnTriggerExit(Collider other)
    {
        CheckExitTrigger(other);
    }

    private void OnTriggerStay(Collider other)
    {
        OnTriggerEnter(other);
    }
    
    protected virtual void CheckEnterTrigger(Collider collider)
    {
        Character character = collider.gameObject.GetComponent<Character>();

        if (character == null || character.IsDead)
        {
            return;
        }

        CharacterInteraction interaction = character.gameObject.GetComponent<CharacterInteraction>();

        if (interaction == null)
        {
            return;
        }

        EnterZone(character);
    }
    
    protected virtual void CheckExitTrigger(Collider collider)
    {
        Character character = collider.gameObject.GetComponent<Character>();

        if (character == null)
        {
            return;
        }

        CharacterInteraction interaction = character.gameObject.GetComponent<CharacterInteraction>();

        if (interaction == null)
        {
            return;
        }

        ExitZone(character);
    }
    
    public void ReplaceInteractionAgents(Character character)
    {
        InteractionAgent = character;
        CharacterInteraction interaction = character.GetComponent<CharacterInteraction>();

        interaction.InInteractionZone = true;
        interaction.Interacting = true;
    }
    
    public virtual void InteractionRequest(Character character)
    {
        if (!Available)
        {
            return;
        }

        if (ActivationType == Activation.Manual
            || (ActivationType == Activation.OnlyFirstAuto && NumberOfActivations > 0))
        {
            Controller controller = character.GetComponent<Controller>();
            if (CanOnlyActivateIfGrounded && !controller.State.IsGrounded)
            {
                return;
            }
        }

        EventManager.TriggerEvent(new BlockCharacter(BlockCharacter.Type.Block));
        TriggerInteraction(character);
    }
    
    public virtual void InteractionExit(Character character)
    {
        StartCoroutine(ExitInteraction(character));
    }
    
    public virtual void TriggerInteraction(Character trigger)
    {
        if (ActivationType == Activation.Manual
            || (ActivationType == Activation.OnlyFirstAuto && NumberOfActivations > 0))
        {
            if(ShowPromptWhenColliding && HidePromptAfterUse)
            {
                m_PromptCo = StartCoroutine(HidePrompt());
            }
        }

        InteractionAgent = trigger;
        m_Interacting = true;

        m_NumberOfActivationsLeft--;
        m_NumberOfActivations++;

        trigger.GetComponent<CharacterInteraction>().Interacting = true;

        EventManager.TriggerEvent(new BlockCharacter(BlockCharacter.Type.Block));
        EventManager.TriggerEvent(new InteractionEvent(InteractionEvent.Type.Start));
        if(NPC != null) EventManager.TriggerEvent(new QuestProgressEvent(QuestProgressEvent.EventType.Talk,NPC.Id));

        OnInteractAction.Invoke(trigger);
    }
    
    public virtual IEnumerator ExitInteraction(Character trigger)
    {
        yield return new WaitForEndOfFrame();

        if (m_Prompt != null && m_LastPromptState)
            ShowPrompt();

        m_Interacting = false;

        if (InteractionAgent != null)
        {
            InteractionAgent.GetComponent<CharacterInteraction>().Interacting = false;
            InteractionAgent = null;
        }

        EventManager.TriggerEvent(new BlockCharacter(BlockCharacter.Type.Unblock));
        EventManager.TriggerEvent(new InteractionEvent(InteractionEvent.Type.End));

        OnInteractExit.Invoke(trigger);
    }
    
    protected virtual bool CheckNumberOfUses()
    {
        if (UnlimitedActivations || m_NumberOfActivationsLeft > 0)
        {
            return true;
        }

        return false;
    }
    
    protected virtual bool CanShowPrompt(Character character)
    {
        if (m_Prompt != null && CheckNumberOfUses())
        {
            return true;
        }
        return false;
    }
    
    protected virtual void ShowPrompt()
    {
        if (m_PromptCo != null)
        {
            StopCoroutine(m_PromptCo);
            m_PromptCo = null;
        }

        if (m_PromptHiddenForever && !ShowPromptWhenColliding)
        {
            return;
        }

        if (m_NumberOfActivationsLeft <= 0 && !UnlimitedActivations)
        {
            return;
        }

        if (m_Prompt.activeInHierarchy) return;

        if (m_Interacting) return;

        if (m_Prompt != null)
        {
            m_Prompt.SetActive(true);
            m_LastPromptState = true;

            m_Prompt.transform.SetParent(transform);
            m_Prompt.transform.localPosition = PromptRelativePosition;

            if (m_Prompt.GetComponent<SpriteRenderer>() != null)
            {
                m_Prompt.GetComponent<SpriteRenderer>().material.color = new Color(1f, 1f, 1f, 0f);
                StartCoroutine(FadeSprite(m_Prompt.GetComponent<SpriteRenderer>(), 0.2f, new Color(1f, 1f, 1f, 1f)));
            }
        }
    }
    
    protected IEnumerator HidePrompt()
    {
        if (m_Prompt != null)
        {
            if (m_Prompt.GetComponent<SpriteRenderer>() != null)
            {
                StartCoroutine(FadeSprite(m_Prompt.GetComponent<SpriteRenderer>(), 0.2f, new Color(1f, 1f, 1f, 0f)));
            }
            yield return new WaitForSeconds(0.3f);
            m_Prompt.SetActive(false);
        }
        else
        {
            yield return null;
        }
    }
    
    public static IEnumerator FadeSprite(SpriteRenderer target, float duration, Color color)
    {
        if (target == null)
            yield break;

        float alpha = target.material.color.a;

        float t = 0f;
        while (t < 1.0f)
        {
            if (target == null)
                yield break;

            Color newColor = new Color(color.r, color.g, color.b, Mathf.SmoothStep(alpha, color.a, t));
            target.material.color = newColor;

            t += Time.deltaTime / duration;

            yield return null;

        }
        Color finalColor = new Color(color.r, color.g, color.b, Mathf.SmoothStep(alpha, color.a, t));
        if (target != null)
        {
            target.material.color = finalColor;
        }
    }
}
