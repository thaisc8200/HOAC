﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum StaminaMethods
{
    Set,
    Add,
    Remove
}

public struct StaminaEvent
{
    public StaminaMethods StaminaMethod;
    public float Quantity;
    public Agent Agent;

    public StaminaEvent(StaminaMethods staminaMethod, float quantity, Agent agent)
    {
        StaminaMethod = staminaMethod;
        Quantity = quantity;
        Agent = agent;
    }
}
public class AgentStamina : MonoBehaviour, EventListener<StaminaEvent>
{
    public bool InfiniteStamina = false;

    public Stat StaminaData;

    public float CurrentStamina { get; set; }
    public float MaxStamina { get; set; }

    private Agent m_Agent;

    private void Awake()
    {
        m_Agent = GetComponent<Agent>();
    }

    void OnEnable()
    {
        this.EventStartListening<StaminaEvent>();
    }

    void OnDisable()
    {
        this.EventStopListening<StaminaEvent>();
    }

    public void Initialize()
    {
        UpdateMaximumStamina();
        Restore();
    }

    public void OnEvent(StaminaEvent eventType)
    {
        if(eventType.Agent != m_Agent)
        {
            return;
        }

        switch (eventType.StaminaMethod)
        {
            case StaminaMethods.Add:
                CurrentStamina = Mathf.Clamp(CurrentStamina + eventType.Quantity, 0, MaxStamina);
                break;
            case StaminaMethods.Remove:
                CurrentStamina = Mathf.Clamp(CurrentStamina - eventType.Quantity, 0, MaxStamina);
                break;
            case StaminaMethods.Set:
                CurrentStamina = Mathf.Clamp(eventType.Quantity, 0, MaxStamina);
                break;
        }

        m_Agent.UpdateStaminaUI();
    }

    public void UpdateMaximumStamina()
    {
        MaxStamina = m_Agent.CurrentStats.Stamina;

        if(CurrentStamina > MaxStamina)
        {
            CurrentStamina = MaxStamina;
        }

        m_Agent.UpdateStaminaUI();
    }

    public void Restore()
    {
        CurrentStamina = MaxStamina;
        m_Agent.UpdateStaminaUI();
    }

    public bool ExpendStamina()
    {
        if (HasStamina())
        {
            if (!InfiniteStamina)
            {
                CurrentStamina -= Time.deltaTime;
                if (CurrentStamina < 0)
                {
                    CurrentStamina = 0;
                    EventManager.TriggerEvent(new AchievementProgressEvent(AchievementProgressEvent.EventType.Stat,
                        StaminaData.Id, (int) CurrentStamina));
                }
            }
            m_Agent.UpdateStaminaUI();

            return true;
        }
        else return false;
    }

    public bool HasStamina()
    {
        return CurrentStamina > 0;
    }

    public void RecoverStamina()
    {
        CurrentStamina += (Time.deltaTime / 2);

        m_Agent.UpdateStaminaUI();
        UpdateMaximumStamina();
    }
}
