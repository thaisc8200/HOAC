﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class EnemySliderBar : SliderBar
{
    public Image WhiteBar;
    public float UpdateTime = 2f;

    public override void UpdateBar(float currentValue, float minValue, float maxValue)
    {
        m_Slider.minValue = minValue;
        m_Slider.maxValue = maxValue;
        if (WhiteBar != null) StartCoroutine(_LerpBar(m_Slider.value, currentValue));
    }
    
    private IEnumerator _LerpBar(float baseValue, float currentValue)
    {
        var elapsedTime = 0f;
        while (elapsedTime < UpdateTime)
        {
            WhiteBar.enabled = true;
            m_Slider.value = Mathf.Lerp(baseValue, currentValue, (elapsedTime / UpdateTime));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        m_Slider.value = currentValue; 
        WhiteBar.enabled = false;
        yield return new WaitForSeconds(1f);
    }
}
