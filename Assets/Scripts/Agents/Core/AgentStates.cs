﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentStates
{
    public enum AgentConditions
    {
        Normal,
        Damaged,
        Dead
    }

    public enum AgentMovementConditions
    {
        Free,
        Fixed,
        Immobilized
    }

    public enum MovementStates
    {
        Null,
        Idle,
        Walking,
        Running,
        Falling,
        Jumping,
        Stealth
    }

    public enum CombatStates
    {
        NoCombat,
        Attacking,
        Recovery,
        Dodging,
        Blocking
    }
}
