﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Effect
{
   protected float m_Duration;
   protected float m_CurrentDuration;

   protected Agent m_Agent;
   protected AgentEffects m_Effects;
   
   public bool Stackable;
   public bool IsAplied { get; set; }

   public bool Expired
   {
      get { return m_CurrentDuration <= 0f; }
   }

   public Effect(float duration, bool stackable)
   {
      m_Duration = duration;
      Stackable = stackable;
   }
   
   public virtual void Apply(Agent agent)
   {
      m_Agent = agent;
      m_Effects = agent.GetComponent<AgentEffects>();

      m_CurrentDuration = m_Duration;
      IsAplied = true;
      UIHUD.Instance.DisplayEffects(m_Effects.CurrentEffects);
   }

   public virtual void Tick()
   {
      m_CurrentDuration -= m_Effects.RefreshRate;

      if (m_CurrentDuration <= 0f)
      {
         Remove();
      }
   }

   public virtual void Remove()
   {
      IsAplied = false;
      UIHUD.Instance.DisplayEffects(m_Effects.CurrentEffects);
   }

   public virtual void Refresh()
   {
      m_CurrentDuration = m_Duration;
   }
}
