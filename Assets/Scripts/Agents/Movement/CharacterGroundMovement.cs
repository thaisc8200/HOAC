﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerInput))]
[RequireComponent(typeof(Character))]
[RequireComponent(typeof(AgentStamina))]
public class CharacterGroundMovement : GroundMovement, EventListener<BlockCharacter>, EventListener<TransitionEvent>
{
   private PlayerInput m_PlayerInput;
   private Character m_Character;
   private AgentStamina m_Stamina;

   [Header("Audio")] 
   public AudioClip GroundWalkSFX;

   protected override void Awake()
   {
      base.Awake();

      m_PlayerInput = GetComponent<PlayerInput>();
      m_Character = m_Agent as Character;
      m_Stamina = GetComponent<AgentStamina>();
   }
   
   protected void OnEnable()
   {
      this.EventStartListening<BlockCharacter>();
      this.EventStartListening<TransitionEvent>();
   }
   
   protected void OnDisable()
   {
      this.EventStopListening<BlockCharacter>();
      this.EventStopListening<TransitionEvent>();
   }

   protected override void EvaluateMovement()
   {
      switch (m_Character.FreedomState.CurrentState)
      {
         case AgentStates.AgentMovementConditions.Free:
            if (!m_PlayerInput.HorizontalAxisNeutral() || !m_PlayerInput.VerticalAxisNeutral())
            {
               HorizontalMove = m_PlayerInput.Horizontal.GetAxis();
               VerticalMove = m_PlayerInput.Vertical.GetAxis();
               CheckRunning();
               CheckOnStealth();
            }
            else
            {
               HorizontalMove = 0f;
               VerticalMove = 0f;
               m_Character.ExitStealth();
               m_Stamina.RecoverStamina();
            }
            break;
         case AgentStates.AgentMovementConditions.Fixed:
            CalculateDirection();
            break;
         case AgentStates.AgentMovementConditions.Immobilized:
            if (m_Controller.State.IsGrounded)
            {
               HorizontalMove = 0f;
               VerticalMove = 0f;
            }
            break;
      }
   }

   private void CheckRunning()
   {
      if (m_PlayerInput.Run.GetButton())
      {
         if (m_Stamina.ExpendStamina()) IsRunning = true;
         else IsRunning = false;
      }
      else
      {
         IsRunning = false;
         m_Stamina.RecoverStamina();
      }
   }

   private void CheckOnStealth()
   {
      if (m_PlayerInput.Stealth.GetButtonDown())
      {
         if (m_Character.MovementState.CurrentState == AgentStates.MovementStates.Stealth)
         {
            IsOnStealth = false;
            m_Character.ExitStealth();
         }
         else
         {
            IsOnStealth = true;
            m_Character.EnterStealth();
            EventManager.TriggerEvent(new QuestProgressEvent(QuestProgressEvent.EventType.Tutorial, (int)m_PlayerInput.Stealth.ActionButton));
         }
      }
   }

   public void PlayWalkSFX()
   {
      if(GroundWalkSFX != null)
      {
         SoundManager.Instance.PlaySound(GroundWalkSFX, transform.position);
      }
   }

   public void OnEvent(BlockCharacter eventType)
   {
      switch (eventType.Block)
      {
         case BlockCharacter.Type.Block:
            DisableMovement();
            break;
         case BlockCharacter.Type.Unblock:
            EnableMovement();
            break;
      }
   }
   
   public void OnEvent(TransitionEvent eventType)
   {
      switch (eventType.TransitionType)
      {
         case TransitionEvent.Type.Stop:
            DisableMovement();
            break;
         case TransitionEvent.Type.End:
            EnableMovement();
            break;
      }
   }
}
