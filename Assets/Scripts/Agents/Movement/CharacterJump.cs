﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(PlayerInput))]
[RequireComponent(typeof(Controller))]
[RequireComponent(typeof(Character))]
public class CharacterJump : MonoBehaviour, EventListener<StateChangeEvent<AgentStates.MovementStates>>
{
    public float JumpHeight = 3;
    
    public float FallTimeException = 0.1f;

    [Header("Audio")] 
    public AudioClip[] JumpAudioClips;
    
    private PlayerInput m_PlayerInput;
    private Controller m_Controller;
    private Character m_Character;
    private Animator m_Animator;

    private float m_FallTime;

    private void Awake()
    {
        m_PlayerInput = GetComponent<PlayerInput>();
        m_Controller = GetComponent<Controller>();
        m_Character = GetComponent<Character>();
        m_Animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        this.EventStartListening<StateChangeEvent<AgentStates.MovementStates>>();
    }

    private void OnDisable()
    {
        this.EventStopListening<StateChangeEvent<AgentStates.MovementStates>>();
    }

    void Update()
    {
        if (GameManager.Instance.Paused || Time.deltaTime == 0) return;

        HandleInput();
        
        if (m_Character.MovementState.CurrentState == AgentStates.MovementStates.Jumping)
        {
            JumpStop();
        }

        if (m_Character.MovementState.CurrentState == AgentStates.MovementStates.Falling)
        {
            m_FallTime += Time.deltaTime;
        }
    }

    private void HandleInput()
    {
        if (m_PlayerInput.Jump.GetButtonDown() && Available())
        {
            StartCoroutine(JumpStart());
            EventManager.TriggerEvent(new QuestProgressEvent(QuestProgressEvent.EventType.Tutorial, (int)m_PlayerInput.Jump.ActionButton));
        }
    }

    public IEnumerator JumpStart()
    {
        if (m_Controller.State.IsGrounded ||
            (m_Character.MovementState.CurrentState == AgentStates.MovementStates.Falling && m_FallTime < FallTimeException))
        {
            m_Character.MovementState.ChangeState(AgentStates.MovementStates.Jumping);
        }

        if (JumpAudioClips != null && JumpAudioClips.Length > 0)
        {
            SoundManager.Instance.PlaySound(JumpAudioClips[Random.Range(0, JumpAudioClips.Length)], transform.position);
        }
        
        m_Controller.SetGravityState(true);

        if (m_Character.MovementState.CurrentState == AgentStates.MovementStates.Jumping)
        {
            m_Controller.SetAerialVelocity(Mathf.Sqrt(2f * JumpHeight * Mathf.Abs(m_Controller.Parameters.Gravity)));
        }
        
        yield return new WaitForSeconds(1f);
        yield return new WaitUntil(() => m_Controller.State.IsGrounded);
        m_Controller.SetAerialVelocity(0f);
    }
    
    private bool Available()
    {
        if (m_Character.ConditionState.CurrentState != AgentStates.AgentConditions.Normal)
        {
            return false;
        }

        if (m_Character.FreedomState.CurrentState != AgentStates.AgentMovementConditions.Free)
        {
            return false;
        }
        
        if (!m_Controller.State.IsGrounded)
        {
            return false;
        }
        return true;
    }

    private void JumpStop()
    {
        m_Character.MovementState.ChangeState(AgentStates.MovementStates.Idle);
    }
    
    public void OnEvent(StateChangeEvent<AgentStates.MovementStates> eventType)
    {
        if (eventType.Target != gameObject
            && eventType.TargetStateMachine != m_Character.MovementState)
        {
            return;
        }

        if (eventType.NewState == AgentStates.MovementStates.Falling)
        {
            m_FallTime = 0f;
        }
    }
}
