﻿using System;
using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

public class CharacterLookAt : MonoBehaviour
{
    public float Speed = 0.5f;
    public Transform Player;
    
    private float m_Horizontal;
    private float m_Vertical;
    private float m_ControllerSpeed;

    private PlayerInput m_PlayerInput;
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        m_PlayerInput = GetComponentInParent<PlayerInput>();
    }
    
    private void Update()
    {
        if (GameManager.Instance.Paused || Time.deltaTime == 0) return;
        
        CameraControl();
    }

    private void CameraControl()
    {
        switch (GameManager.GetControllerType())
        {
            case ControllerType.Joystick:
                m_ControllerSpeed = 2f;
                break;
            case ControllerType.Mouse:
                m_ControllerSpeed = 0.2f;
                break;
            default:
                m_ControllerSpeed = 1f;
                break;
        }
        if (!m_PlayerInput.RightHorizontalAxisNeutral() || !m_PlayerInput.RightVerticalAxisNeutral())
        {
            m_Horizontal += m_PlayerInput.LookAtHorizontal.GetAxis() * Speed * m_ControllerSpeed;
            m_Vertical -= m_PlayerInput.LookAtVertical.GetAxis() * Speed * m_ControllerSpeed;
            m_Vertical = Mathf.Clamp(m_Vertical, -15, 35);
            
            transform.rotation = Quaternion.Euler(m_Vertical, m_Horizontal, 0);
            Player.rotation = Quaternion.Euler(0, m_Horizontal, 0);
        }
    }
}
