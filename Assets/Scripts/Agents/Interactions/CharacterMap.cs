﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMap : MonoBehaviour
{
    private PlayerInput m_PlayerInput;
    private Character m_Character;

    private void Awake()
    {
        m_PlayerInput = GetComponent<PlayerInput>();
        m_Character = GetComponent<Character>();
    }

    private void Update()
    {
        if(m_Character.IsDead) return;

        if (GUIManager.Instance.MapCanvas.gameObject.activeInHierarchy)
        {
            if (m_PlayerInput.Map.GetButtonDown() || m_PlayerInput.Back.GetButtonDown())
            {
                GUIManager.Instance.CloseMap();
            }
        }
        else
        {
            if (GameManager.Instance.Paused || Time.deltaTime == 0f)
            {
                return;
            }

            if (m_PlayerInput.Map.GetButtonDown())
            {
                GUIManager.Instance.OpenMap(transform);
                EventManager.TriggerEvent(new QuestProgressEvent(QuestProgressEvent.EventType.Tutorial, (int)m_PlayerInput.Map.ActionButton));
            }
        }
    }
}
