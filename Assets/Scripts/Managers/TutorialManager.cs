﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : PersistentSingleton<TutorialManager>
{
    public GameObject Collider;
    public Quest LastQuest;
    public Interaction Interaction;
    
    public bool IsCompleted { get; set; }

    private void Start()
    {
        if (QuestManager.Instance.QuestFinished(LastQuest)) IsCompleted = true;
    }

    private void Update()
    {
        if(!IsCompleted) TutorialCompleted();
    }

    public void TutorialCompleted()
    {
        if (QuestManager.Instance.QuestFinished(LastQuest))
        {
            Collider.SetActive(false);
            Collider.GetComponent<Collider>().enabled = false;
            IsCompleted = true;
            EventManager.TriggerEvent(new AchievementProgressEvent(AchievementProgressEvent.EventType.Interact, Interaction.Id));
        }
    }
}
