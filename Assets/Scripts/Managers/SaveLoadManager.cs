﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor;
using UnityEngine;

public static class SaveLoadManager
{
   private const string BASE_FOLDER_NAME = "/SaveData/";
    private const string DEFAULT_FOLDER_NAME = "SaveManager";
    private const string DEFAULT_FILE_NAME = "Save";

    public static bool SaveExists(string fileName = DEFAULT_FILE_NAME, string folderName = DEFAULT_FOLDER_NAME)
    {
        string savePath = DetermineSavePath(folderName);
        string saveFileName = DetermineSaveFileName(fileName);
        if (File.Exists(savePath+saveFileName)) {
            return true;
        } else {
            return false;
        }
    }

    private static string DetermineSavePath(string folderName = DEFAULT_FOLDER_NAME)
    {
        string savePath;

        savePath = Application.dataPath + BASE_FOLDER_NAME;

        savePath = savePath + folderName + "/";
        return savePath;
    }

    private static string DetermineSaveFileName(string fileName)
    {
        return fileName + ".game";
    }

    public static void SaveGame(object saveObject, string fileName = DEFAULT_FILE_NAME, string foldername = DEFAULT_FOLDER_NAME)
    {
        string savePath = DetermineSavePath(foldername);
        string saveFileName = DetermineSaveFileName(fileName);

        if (!Directory.Exists(savePath)) {
            Directory.CreateDirectory(savePath);
        }

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream saveFile = new FileStream(savePath + saveFileName, FileMode.Create);

        Debug.Log(savePath + saveFileName);

        formatter.Serialize(saveFile, saveObject);
        saveFile.Close();
    }

    public static object LoadGame(string fileName = DEFAULT_FILE_NAME, string foldername = DEFAULT_FOLDER_NAME)
    {
        string savePath = DetermineSavePath(foldername);
        string saveFileName = savePath + DetermineSaveFileName(fileName);

        object loadData;
        
        if (!Directory.Exists(savePath) || !File.Exists(saveFileName)) {
            throw new SavedGameNotFoundException(saveFileName);
        }

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(saveFileName, FileMode.Open);
        loadData = formatter.Deserialize(stream);
        stream.Close();
        
        return loadData;
    }

    public static void DeleteSave(string fileName = DEFAULT_FILE_NAME, string folderName = DEFAULT_FOLDER_NAME)
    {
        string savePath = DetermineSavePath(folderName);
        string saveFileName = DetermineSaveFileName(fileName);
        File.Delete(savePath + saveFileName);
    }

    public static void DeleteSaveFolder(string folderName = DEFAULT_FOLDER_NAME)
    {
        string savePath = DetermineSavePath(folderName);
        Directory.Delete(savePath,true);
    }
}

public class SavedGameNotFoundException : Exception
{
    public SavedGameNotFoundException(string path) : base(path)
    {

    }
}

