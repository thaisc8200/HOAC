﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JournalEntryStatus
{
    public JournalEntry Entry;
    public bool IsAchieved;

    public JournalEntryStatus(JournalEntry entry, bool isAchieved)
    {
        Entry = entry;
        IsAchieved = isAchieved;
    }
    
    public JournalEntryStatus(JournalEntry entry)
    {
        Entry = entry;
        IsAchieved = false;
    }
    
    public void UpdateProgressComplete(int entryID)
    {
        if(IsAchieved) return;

        if (Entry.Id == entryID)
        {
            IsAchieved = true;
            EventManager.TriggerEvent(new JournalEntryEvent(true, Entry));
        }
    }
}
