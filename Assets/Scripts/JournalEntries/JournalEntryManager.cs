﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public struct JournalEntryProgressEvent
{
    public int ID;

    public JournalEntryProgressEvent(int id)
    {
        ID = id;
    }
}

public struct JournalEntryEvent
{
    public bool IsAchieved;
    public JournalEntry Entry;

    public JournalEntryEvent(bool isAchieved, JournalEntry entry)
    {
        IsAchieved = isAchieved;
        Entry = entry;
    }
}
public class JournalEntryManager : PersistentSingleton<JournalEntryManager>, EventListener<JournalEntryProgressEvent>
{
    public List<JournalEntry> JournalEntries;

    public List<JournalEntryStatus> Entries { get; set; }

    protected override void Awake()
    {
        base.Awake();
        
        Entries = new List<JournalEntryStatus>();
    }

    private void Start()
    {
        foreach (var entry in JournalEntries)
        {
            JournalEntryStatus entryStatus = Entries.Find(x => entry.Id == x.Entry.Id);

            if (entryStatus == null)
            {
                entryStatus = new JournalEntryStatus(entry);
                Entries.Add(entryStatus);
            }
        }
    }
    
    void OnEnable()
    {
        this.EventStartListening<JournalEntryProgressEvent>();
    }
    
    void OnDisable()
    {
        this.EventStopListening<JournalEntryProgressEvent>();
    }

    private void UpdateEntry(int entryID)
    {
        List<JournalEntryStatus> nonAchieved = new EntryNonAchievedFilter().Filter(Entries);
        nonAchieved.ForEach(x => x.UpdateProgressComplete(entryID));
    }

    public void OnEvent(JournalEntryProgressEvent eventType)
    {
        UpdateEntry(eventType.ID);
    }
}

public interface JournalEntryFilter
{
    List<JournalEntryStatus> Filter(List<JournalEntryStatus> toFilter);
}

public class EntryAchievedFilter : JournalEntryFilter
{
    public EntryAchievedFilter()
    {

    }

    public List<JournalEntryStatus> Filter(List<JournalEntryStatus> toFilter)
    {
        return toFilter.Where(x => x.IsAchieved).ToList();
    }
}

public class EntryNonAchievedFilter : JournalEntryFilter
{
    public EntryNonAchievedFilter()
    {

    }

    public List<JournalEntryStatus> Filter(List<JournalEntryStatus> toFilter)
    {
        return toFilter.Where(x => !x.IsAchieved).ToList();
    }
}
