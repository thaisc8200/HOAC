﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct InventoryEvent
{
    public enum Type { Add, Remove }

    public Type EventType;

    public Item Item;
    public int Quantity;

    public InventoryEvent(Type type, Item item, int quantity)
    {
        EventType = type;
        Item = item;
        Quantity = quantity;
    }
}

[Serializable]
public struct SerializedInventory
{
    public List<SerializedListedItem> Items;
}

[Serializable]
public struct SerializedListedItem
{
    public int ID;
    public int Quantity;
}
public class Inventory : PersistentSingleton<Inventory>, ISerialize<SerializedInventory>
{
    public enum Category
    {
        All,
        Consumable,
        Usable
    }
    
    public List<ListedItem> Items;
    
    public void AddItem(Item item, int quantity)
    {
        ListedItem invItem = FindItem(item);
        if (invItem != null)
        {
            invItem.Quantity = Mathf.Clamp(invItem.Quantity + quantity, 0, item.MaxQuantity);
        }
        else
        {
            invItem = new ListedItem(item, Mathf.Clamp(quantity, 0, item.MaxQuantity));
            Items.Add(invItem);
        }

        EventManager.TriggerEvent(new InventoryEvent(InventoryEvent.Type.Add, item, quantity));
    }
    
    public void RemoveItem(Item item, int quantity)
    {
        ListedItem invItem = FindItem(item);
        if (invItem != null)
        {
            invItem.Quantity -= quantity;
            if (invItem.Quantity <= 0)
            {
                Items.Remove(invItem);
            }
        }

        EventManager.TriggerEvent(new InventoryEvent(InventoryEvent.Type.Remove, item, quantity));
    }
    
    public void Clear()
    {
        Items.Clear();
    }
    
    public void Sort()
    {
        Items.Sort(new IDComparer());
    }
    
    public List<ListedItem> GetCategoryItems(Category category)
    {
        List<ListedItem> filteredList = new List<ListedItem>();
        for (int i = 0; i < Items.Count; i++)
        {
            switch (category)
            {
                case Category.All:
                    filteredList.Add(Items[i]);
                    break;
                case Category.Consumable:
                    if (Items[i].Item is ConsumableItem) filteredList.Add(Items[i]);
                    break;
                case Category.Usable:
                    if (Items[i].Item is UsableItem && !(Items[i].Item is ConsumableItem)) 
                        filteredList.Add(Items[i]);
                    break;
            }
        }
        return filteredList;
    }
    
    public ListedItem FindItem(Item item)
    {
        return Items.Find(x => x.Item == item);
    }
    
    public SerializedInventory Serialize()
    {
        SerializedInventory inventory = new SerializedInventory
        {
            Items = new List<SerializedListedItem>()
        };

        foreach (var element in Items)
        {
            SerializedListedItem item = new SerializedListedItem
            {
                ID = element.Item.ID,
                Quantity = element.Quantity
            };

            inventory.Items.Add(item);
        }

        return inventory;
    }

    /// <summary>
    /// Deserialize
    /// </summary>
    /// <param name="data"></param>
    public void Deserialize(SerializedInventory inventory)
    {
        Items = new List<ListedItem>();

        for (int i = 0; i < inventory.Items.Count; i++)
        {
            try
            {
                Items.Add(new ListedItem(ItemDB.Instance.GetById(inventory.Items[i].ID), inventory.Items[i].Quantity));
            }
            catch (Exception)
            {
                Debug.Log("item deserialization error");
            }
        }
    }
}

[Serializable]
public class ListedItem
{
    public Item Item;
    public int Quantity;

    public ListedItem(Item item, int qty)
    {
        Item = item;
        Quantity = qty;
    }
}

public class IDComparer : IComparer<ListedItem>
{
    public int Compare(ListedItem x, ListedItem y)
    {
        if (x.Item.ID == y.Item.ID)
        {
            return 0;
        }
        else if (x.Item.ID < y.Item.ID)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
}
