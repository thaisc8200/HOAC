﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemDB", menuName = "G1/Database/ItemDB")]
public class ItemDB : AbstractDatabase<Item>
{
    private static ItemDB m_Instance = null;

    public static ItemDB Instance
    {
        get
        {
            if (m_Instance == null)
            {
                LoadDatabase();
            }

            return m_Instance;
        }
    }
    
    protected override void OnAddObject(Item t)
    {
#if UNITY_EDITOR

#endif
    }

    protected override void OnRemoveObject(Item t)
    {
#if UNITY_EDITOR
        AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(t));
        AssetDatabase.SaveAssets();
        EditorUtility.SetDirty(this);
#endif
    }
    
    public static void LoadDatabase()
    {
#if UNITY_EDITOR
        m_Instance = (ItemDB)AssetDatabase.LoadAssetAtPath("Assets/ScriptableObjects/Model/ItemDB.asset", typeof(ItemDB));
#else
            AssetBundle bundle = MyAssetBundle.LoadAssetBundleFromFile("g1/items");
            m_Instance = bundle.LoadAsset<ItemDB>("ItemDB");
            bundle.Unload(false);
#endif
    }

    public static void ClearDatabase()
    {
        m_Instance = null;
    }
}
