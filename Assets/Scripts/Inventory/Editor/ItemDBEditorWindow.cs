﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class ItemDBEditorWindow
{
    private static Vector2 m_Scroll;
        private static Item m_HoveredItem;

        public static void DrawWindow()
        {
            Controls();

            EditorGUILayout.BeginVertical(GUI.skin.box, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("ItemDB");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            m_Scroll = EditorGUILayout.BeginScrollView(m_Scroll, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

            DrawHeaders();

            Event e = Event.current;
            if (e.type != EventType.Layout)
            {
                m_HoveredItem = null;
            }

            for (int i = 0; i < ItemDB.Instance.RowCount; i++)
            {
                DrawItem(i, ItemDB.Instance.GetAtIndex(i));
            }

            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();

            if (GUI.changed)
            {
                EditorUtility.SetDirty(ItemDB.Instance);
            }
        }

        private static void Controls()
        {
            Event e = Event.current;
            switch (e.type)
            {
                case EventType.MouseUp:
                    if (e.button == 1)
                    {
                        GenericMenu menu = new GenericMenu();
                        if (m_HoveredItem != null)
                        {
                            menu.AddItem(new GUIContent("Remove"), false, () => RemoveItem(m_HoveredItem));
                        }

                        menu.DropDown(new Rect(Event.current.mousePosition, Vector2.zero));
                    }
                    break;
            }
        }

        private static void DrawHeaders()
        {
            GUILayout.BeginHorizontal("box");

            GUILayout.BeginHorizontal("box", GUILayout.Width(150));
            GUILayout.Label("ID");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Width(250));
            GUILayout.Label("Name");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Width(300));
            GUILayout.Label("Description");
            GUILayout.EndHorizontal();

            GUILayout.EndHorizontal();
        }

        private static void DrawItem(int i, Item item)
        {
        if (item == null)
        {
            return;
        }

        SerializedObject target = new SerializedObject(item);

        EditorGUIUtility.labelWidth = 75;

        GUILayout.BeginHorizontal("box", GUILayout.Height(75));

        // ID
        EditorGUILayout.BeginVertical("box", GUILayout.Width(150), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
        EditorGUILayout.PropertyField(target.FindProperty("ID"));
        EditorGUILayout.EndVertical();

        // Name
        EditorGUILayout.BeginVertical("box", GUILayout.Width(250), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
        EditorGUILayout.PropertyField(target.FindProperty("ItemName"));
        EditorGUILayout.Space();
        //if (!string.IsNullOrEmpty(item.Name)) EditorGUILayout.LabelField(StaticLocalizationManager.Translation("ITEM", item.Name), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
        EditorGUILayout.EndVertical();

        // Description
        GUILayout.BeginVertical("box", GUILayout.Width(300), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
        EditorGUILayout.PropertyField(target.FindProperty("Description"), true);
        EditorGUILayout.Space();
        //if (!string.IsNullOrEmpty(item.Name)) EditorGUILayout.LabelField(StaticLocalizationManager.Translation("ITEM", item.Description), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
        GUILayout.EndVertical();

        GUILayout.EndHorizontal();

        Event e = Event.current;
        Vector2 mousePos = Event.current.mousePosition;


        if (e.type != EventType.Layout)
        {
            Rect windowRect = new Rect(GUILayoutUtility.GetLastRect().position, GUILayoutUtility.GetLastRect().size);
            if (windowRect.Contains(mousePos)) m_HoveredItem = item;
        }

        target.ApplyModifiedProperties();
    }

    private static void AddNewEnemy()
    {
        EditorUtility.SetDirty(ItemDB.Instance);
        Item item = ScriptableObject.CreateInstance<Item>();
        item.Id = ItemDB.Instance.GetFirstAvalibleId();
        ItemDB.Instance.Add(item);
    }

    private static void RemoveItem(int index)
    {
        EditorUtility.SetDirty(ItemDB.Instance);
        ItemDB.Instance.RemoveAt(index);
    }

    private static void RemoveItem(Item item)
    {
        EditorUtility.SetDirty(ItemDB.Instance);
        ItemDB.Instance.Remove(item);
    }
}

