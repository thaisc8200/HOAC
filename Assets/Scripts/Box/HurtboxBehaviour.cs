﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtboxBehaviour : BoxBehaviour
{
    public Health Health { get; set; }

    protected override void Awake()
    {
        base.Awake();

        Health = GetComponentInParent<Health>();
    }
}
