﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class HitboxEvent : UnityEvent<Collider>
{

}
public class HitboxBehaviour : BoxBehaviour
{
    public HitboxEvent OnHurtboxHit;
    
    protected HitTechnique m_Technique;
    
    public HitTechnique Technique { get { return m_Technique; } }
    
    public Agent Agent { get; set; }

    protected List<HurtboxBehaviour> m_HurtboxDamaged;

    protected override void Awake()
    {
        base.Awake();
        
        m_HurtboxDamaged = new List<HurtboxBehaviour>();
    }

    void OnEnable()
    {
        m_HurtboxDamaged.Clear();
    }

    private void Update()
    {
        HitTechnique hit = m_Technique as HitTechnique;
        if (hit == null) return;

        Collider[] hits = Physics.OverlapBox(Collider.bounds.center, Collider.bounds.extents, Collider.transform.rotation, LayerMask.GetMask("Hurtbox"));
        foreach(var collision in hits)
        {
            if (collision.transform.parent == transform.parent)
            {
                continue;
            }

            HurtboxBehaviour hurtbox = collision.GetComponent<HurtboxBehaviour>();
            if (hurtbox == null) continue;
            
            
            if (!m_HurtboxDamaged.Contains(hurtbox))
            {
                if (hit.Damage.Target.Contains(hurtbox.BaseGameObject.tag))
                {
                    OnHurtboxHit.Invoke(collision);
                    m_Technique.TechniqueHit(Collider, collision);
                    if ((m_Technique as HitTechnique).SingleHit) m_HurtboxDamaged.Add(hurtbox);
                }
            }
        }
    }

    public void SetTechnique(HitTechnique technique)
    {
        m_Technique = technique;
    }
    
    public void SetAgent(Agent owner)
    {
        Agent = owner;

        HurtboxBehaviour ownHurtbox = Agent.GetComponentInChildren<HurtboxBehaviour>();
        if (ownHurtbox != null && !m_HurtboxDamaged.Contains(ownHurtbox)) m_HurtboxDamaged.Add(ownHurtbox);
    }
}
