﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

[Serializable]
public class AskQuestEvent : UnityEvent<Character>
{
    
}
[RequireComponent(typeof(Interactable))]
public class NPCQuest : MonoBehaviour, EventListener<DialogueEvent>
{
    [Header("Data")] 
    public DialogueGraph Dialogue;
    public List<Quest> Quests;

    [Header("Offer Quest Text")]
    public string MessageText;
    
    [Header("AskQuestEvent")] 
    public AskQuestEvent OnAskOpen;
    public AskQuestEvent OnAskClose;
    
    public NPC NPC { get; set; }
    public Quest CurrentQuest { get; set; }

    private Interactable m_Interactable;
    
    private void Awake()
    {
        m_Interactable = GetComponent<Interactable>();
        NPC = m_Interactable.NPC;
    }
    
    void OnEnable()
    {
        this.EventStartListening<DialogueEvent>();
    }
    
    void OnDisable()
    {
        this.EventStopListening<DialogueEvent>();
    }

    public void AskQuest(Character character)
    {
        GUIManager.Instance.OpenDialogue(Dialogue,character.transform,transform, m_Interactable.NumberOfActivations);
        OnAskOpen.Invoke(character);
    }

    public void CloseAskQuest(Transform opener)
    {
        Character character = opener.GetComponent<Character>();
        OnAskClose.Invoke(character);
    }
    
    public void OnEvent(DialogueEvent eventType)
    {
        if (eventType.Graph == Dialogue && eventType.Type == DialogueEvent.EventType.End)
        {
            Character character = eventType.Opener.GetComponent<Character>();
            OnAskClose.Invoke(character);
        }
    }
}
