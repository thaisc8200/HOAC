﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Achievement", menuName = "G1/Diary/Achievement")]
public class Achievement : ScriptableObject, IDatabaseAsset
{
   public int AchievementID;
   public string AchievementNameTextKey;
   public string AchievementDescriptionTextKey;
   public Sprite AchievementImage;
   
   public List<KillAchievementGoal> EnemyAmount;
   public List<DieAchievementGoal> DeathAmount;
   public List<StatAchievementGoal> StatAmount;
   public List<UseItemAchievementGoal> ItemAmount;
   public List<NPCAchievementGoal> ActivateNpcID;
   public List<ZoneAchievementGoal> ActivateZoneID;
   public List<InteractTriggerAchievementGoal> Interactions;

   public int Id {
      get { return AchievementID; }
      set { AchievementID = value; }
   }

   public string Name
   {
      get { return AchievementNameTextKey;}
      set { AchievementNameTextKey = value; }
   }
}

public abstract class AchievementGoal
{
   public int ID;
   public string Name;

   public AchievementGoal(int id, string name)
   {
      ID = id;
      Name = name;
   }
}

[Serializable]
public class KillAchievementGoal : AchievementGoal
{
   public int RequiredAmount;

   public KillAchievementGoal(int enemyID, string name, int required) : base(enemyID, name)
   {
      RequiredAmount = required;
   }
}

[Serializable]
public class DieAchievementGoal : AchievementGoal
{
   public int RequiredAmount;

   public DieAchievementGoal(int enemyID, string name, int required) : base(enemyID, name)
   {
      RequiredAmount = required;
   }
}

[Serializable]
public class StatAchievementGoal : AchievementGoal
{
   public int RequiredAmount;

   public StatAchievementGoal(int objectID, string name, int required) : base(objectID, name)
   {
      RequiredAmount = required;
   }
}

[Serializable]
public class UseItemAchievementGoal : AchievementGoal
{
   public int RequiredAmount;

   public UseItemAchievementGoal(int itemID, string name, int required) : base(itemID, name)
   {
      RequiredAmount = required;
   }
}

[Serializable]
public class NPCAchievementGoal : AchievementGoal
{
   public NPCAchievementGoal(int npcID, string name) : base(npcID, name)
   {

   }
}

[Serializable]
public class ZoneAchievementGoal : AchievementGoal
{
   public ZoneAchievementGoal(int zoneID, string name) : base(zoneID, name)
   {

   }
}

[Serializable]
public class InteractTriggerAchievementGoal : AchievementGoal
{
   public InteractTriggerAchievementGoal(int triggerID, string name) : base(triggerID, name)
   {

   }
}
