﻿using UnityEngine;

[CreateAssetMenu(fileName = "NPC", menuName = "G1/Diary/Quest System/NPC")]
public class NPC : ScriptableObject, IDatabaseAsset
{
    public int ID;

    public string NameKey;

    public Sprite DialoguePortrait;

    public int Id { get { return ID; } set { ID = value; } }
    public string Name { get { return NameKey; } set { NameKey = value; } }
}
