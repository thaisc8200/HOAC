﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "JournalEntry", menuName = "G1/Diary/Journal Entry")]
public class JournalEntry : ScriptableObject, IDatabaseAsset
{
    public int EntryID;
    public string NameKey;

    public int Id
    {
        get => EntryID;
        set => EntryID = value;
    }
    public string Name
    {
        get => NameKey;
        set => NameKey = value;
    }
}
