﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AchievementDBEditorWindow : EditorWindow
{
    private static Vector2 m_Scroll;
    private static Achievement m_HoveredAchievement;

    public static void DrawWindow()
    {
        Controls();

        EditorGUILayout.BeginVertical(GUI.skin.box, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label("AchievementDB");
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        m_Scroll = EditorGUILayout.BeginScrollView(m_Scroll, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

        DrawHeaders();

        Event e = Event.current;
        if (e.type != EventType.Layout)
        {
            m_HoveredAchievement = null;
        }

        for (int i = 0; i < AchievementDB.Instance.RowCount; i++)
        {
            DrawAchievement(i, AchievementDB.Instance.GetAtIndex(i));
        }

        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndVertical();

        if (GUI.changed)
        {
            EditorUtility.SetDirty(AchievementDB.Instance);
        }
    }

    private static void Controls()
    {
        Event e = Event.current;
        switch (e.type)
        {
            case EventType.MouseUp:
                if (e.button == 1)
                {
                    GenericMenu menu = new GenericMenu();
                    if (m_HoveredAchievement != null)
                    {
                        menu.AddItem(new GUIContent("Remove"), false,
                            () => RemoveAchievement(m_HoveredAchievement));
                    }

                    menu.DropDown(new Rect(Event.current.mousePosition, Vector2.zero));
                }

                break;
        }
    }

    private static void DrawHeaders()
    {
        GUILayout.BeginHorizontal("box");

        GUILayout.BeginHorizontal("box", GUILayout.Width(150));
        GUILayout.Label("ID");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal("box", GUILayout.Width(250));
        GUILayout.Label("Name");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal("box", GUILayout.Width(300));
        GUILayout.Label("Description");
        GUILayout.EndHorizontal();
        
        GUILayout.BeginHorizontal("box", GUILayout.MinWidth(300));
        GUILayout.Label("Sprite");
        GUILayout.EndHorizontal();

        GUILayout.EndHorizontal();
    }

    private static void DrawAchievement(int i, Achievement item)
    {
        if (item == null)
        {
            return;
        }

        SerializedObject target = new SerializedObject(item);

        EditorGUIUtility.labelWidth = 75;

        GUILayout.BeginHorizontal("box", GUILayout.Height(75));

        // ID
        EditorGUILayout.BeginVertical("box", GUILayout.Width(150), GUILayout.ExpandHeight(true),
            GUILayout.ExpandWidth(true));
        EditorGUILayout.PropertyField(target.FindProperty("AchievementID"));
        EditorGUILayout.EndVertical();

        // Name
        EditorGUILayout.BeginVertical("box", GUILayout.Width(250), GUILayout.ExpandHeight(true),
            GUILayout.ExpandWidth(true));
        EditorGUILayout.PropertyField(target.FindProperty("AchievementNameTextKey"));
        EditorGUILayout.Space();
        //if (!string.IsNullOrEmpty(item.Name)) EditorGUILayout.LabelField(StaticLocalizationManager.Translation("ITEM", item.Name), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
        EditorGUILayout.EndVertical();

        // Description
        GUILayout.BeginVertical("box", GUILayout.Width(300), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
        EditorGUILayout.PropertyField(target.FindProperty("AchievementDescriptionTextKey"), true);
        EditorGUILayout.Space();
        //if (!string.IsNullOrEmpty(item.Name)) EditorGUILayout.LabelField(StaticLocalizationManager.Translation("ITEM", item.Description), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
        GUILayout.EndVertical();
        
        GUILayout.BeginVertical("box", GUILayout.Width(300), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
        EditorGUILayout.PropertyField(target.FindProperty("AchievementImage"), GUILayout.MinWidth(100));
        EditorGUILayout.Space();
        GUILayout.EndVertical();

        GUILayout.EndHorizontal();

        Event e = Event.current;
        Vector2 mousePos = Event.current.mousePosition;


        if (e.type != EventType.Layout)
        {
            Rect windowRect = new Rect(GUILayoutUtility.GetLastRect().position, GUILayoutUtility.GetLastRect().size);
            if (windowRect.Contains(mousePos)) m_HoveredAchievement = item;
        }

        target.ApplyModifiedProperties();
    }

    private static void AddAchievement()
    {
        EditorUtility.SetDirty(AchievementDB.Instance);
        Achievement achievement = ScriptableObject.CreateInstance<Achievement>();
        achievement.Id = AchievementDB.Instance.GetFirstAvalibleId();
        AchievementDB.Instance.Add(achievement);
    }

    private static void RemoveAchievement(int index)
    {
        EditorUtility.SetDirty(AchievementDB.Instance);
        AchievementDB.Instance.RemoveAt(index);
    }

    private static void RemoveAchievement(Achievement achievement)
    {
        EditorUtility.SetDirty(AchievementDB.Instance);
        AchievementDB.Instance.Remove(achievement);
    }
}
