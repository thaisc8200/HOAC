﻿using System.Collections;
using System.Collections.Generic;
using PetoonsStudio.Tools;
using UnityEditor;
using UnityEngine;

public static class ZoneDBEditorWindow
{
    private static Vector2 m_Scoll;
        private static Zone m_HoveredZone;

        public static void DrawWindow()
        {
            Controls();

            EditorGUILayout.BeginVertical(GUI.skin.box, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Zone DB");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            m_Scoll = EditorGUILayout.BeginScrollView(m_Scoll, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

            DrawHeaders();

            Event e = Event.current;
            if (e.type != EventType.Layout)
            {
                m_HoveredZone = null;
            }

            if (ZoneDB.Instance.RowCount > 0)
            {
                for (int i = 0; i < ZoneDB.Instance.Count; i++)
                {
                    DrawZone(i, ZoneDB.Instance.GetAtIndex(i));
                }
            }

            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();

            if (GUI.changed)
            {
                EditorUtility.SetDirty(ZoneDB.Instance);
            }
        }

        private static void Controls()
        {
            Event e = Event.current;
            switch (e.type)
            {
                case EventType.MouseUp:
                    if (e.button == 1)
                    {
                        GenericMenu menu = new GenericMenu();
                        if (m_HoveredZone != null)
                        {
                            menu.AddItem(new GUIContent("Remove"), false, () => RemoveZone(m_HoveredZone));
                        }
                        menu.AddItem(new GUIContent("Add"), false, () => AddNewZone());
                        menu.DropDown(new Rect(Event.current.mousePosition, Vector2.zero));
                    }
                    break;
            }
        }

        private static void DrawHeaders()
        {
            GUILayout.BeginHorizontal("box");

            GUILayout.BeginHorizontal("box", GUILayout.Width(75));
            GUILayout.Label("ID");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Width(250));
            GUILayout.Label("Name");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box");
            GUILayout.Label("TitlePrefab");
            GUILayout.EndHorizontal();

            GUILayout.EndHorizontal();
        }

        private static void DrawZone(int i, Zone zone)
        {
            SerializedObject target = new SerializedObject(zone);

            GUILayout.BeginHorizontal("box", GUILayout.Height(75));

            GUILayout.BeginHorizontal("box", GUILayout.Width(75), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            GUILayout.Label(zone.Id.ToString(), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false), GUILayout.MaxWidth(67));
            GUILayout.EndHorizontal();

            EditorGUILayout.BeginVertical("box", GUILayout.Width(250), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(target.FindProperty("NameKey"));
            EditorGUILayout.Space();
            if (!string.IsNullOrEmpty(zone.Name)) EditorGUILayout.LabelField(StaticLocalizationManager.Translation("LOCATION", zone.Name), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
            EditorGUILayout.EndVertical();

            GUILayout.BeginVertical("box", GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(target.FindProperty("ZoneTittlePrefab"), true);
            GUILayout.EndVertical();

            GUILayout.EndHorizontal();

            Event e = Event.current;
            Vector2 mousePos = Event.current.mousePosition;

            if (e.type != EventType.Layout)
            {
                Rect windowRect = new Rect(GUILayoutUtility.GetLastRect().position, GUILayoutUtility.GetLastRect().size);
                if (windowRect.Contains(mousePos)) m_HoveredZone = zone;
            }

            target.ApplyModifiedProperties();
        }

        private static void AddNewZone()
        {
            EditorUtility.SetDirty(ZoneDB.Instance);
            Zone zone = ScriptableObject.CreateInstance<Zone>();
            zone.Id = ZoneDB.Instance.GetFirstAvalibleId();
            ZoneDB.Instance.Add(zone);
        }

        private static void RemoveZone(int index)
        {
            EditorUtility.SetDirty(ZoneDB.Instance);
            ZoneDB.Instance.RemoveAt(index);
        }

        private static void RemoveZone(Zone zone)
        {
            EditorUtility.SetDirty(ZoneDB.Instance);
            ZoneDB.Instance.Remove(zone);
        }
}