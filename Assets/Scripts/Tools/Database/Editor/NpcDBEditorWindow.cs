﻿using System.Collections;
using System.Collections.Generic;
using PetoonsStudio.Tools;
using UnityEditor;
using UnityEngine;

public static class NpcDBEditorWindow
{
    private static Vector2 m_Scoll;
        private static NPC m_HoveredNpc;

        public static void DrawWindow()
        {
            Controls();

            EditorGUILayout.BeginVertical(GUI.skin.box, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("NPC DB");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            m_Scoll = EditorGUILayout.BeginScrollView(m_Scoll, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

            DrawHeaders();

            Event e = Event.current;
            if (e.type != EventType.Layout)
            {
                m_HoveredNpc = null;
            }

            if (NpcDB.Instance.RowCount > 0)
            {
                for (int i = 0; i < NpcDB.Instance.Count; i++)
                {
                    DrawNpc(i, NpcDB.Instance.GetAtIndex(i));
                }
            }

            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();

            if (GUI.changed)
            {
                EditorUtility.SetDirty(NpcDB.Instance);
            }
        }

        private static void Controls()
        {
            Event e = Event.current;
            switch (e.type)
            {
                case EventType.MouseUp:
                    if (e.button == 1)
                    {
                        GenericMenu menu = new GenericMenu();
                        if (m_HoveredNpc != null)
                        {
                            menu.AddItem(new GUIContent("Remove"), false, () => RemoveNpc(m_HoveredNpc));
                        }
                        menu.AddItem(new GUIContent("Add"), false, () => AddNewNpc());
                        menu.DropDown(new Rect(Event.current.mousePosition, Vector2.zero));
                    }
                    break;
            }
        }

        private static void DrawHeaders()
        {
            GUILayout.BeginHorizontal("box");

            GUILayout.BeginHorizontal("box", GUILayout.Width(75));
            GUILayout.Label("ID");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box");
            GUILayout.Label("Name");
            GUILayout.EndHorizontal();

            GUILayout.EndHorizontal();
        }

        private static void DrawNpc(int i, NPC npc)
        {
            GUILayout.BeginHorizontal("box", GUILayout.Height(75));

            GUILayout.BeginHorizontal("box", GUILayout.Width(75), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            GUILayout.Label(npc.Id.ToString(), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false), GUILayout.MaxWidth(67));
            GUILayout.EndHorizontal();

            GUILayout.BeginVertical("box", GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            EditorGUIUtility.labelWidth = 50;
            npc.Name = EditorGUILayout.TextField("KEY", npc.Name, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false));
            EditorGUILayout.LabelField(StaticLocalizationManager.Translation("NPC", npc.Name), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
            GUILayout.EndVertical();

            GUILayout.EndHorizontal();

            Event e = Event.current;
            Vector2 mousePos = Event.current.mousePosition;

            if (e.type != EventType.Layout)
            {
                Rect windowRect = new Rect(GUILayoutUtility.GetLastRect().position, GUILayoutUtility.GetLastRect().size);
                if (windowRect.Contains(mousePos)) m_HoveredNpc = npc;
            }
        }

        private static void AddNewNpc()
        {
            EditorUtility.SetDirty(NpcDB.Instance);
            NPC npc = ScriptableObject.CreateInstance<NPC>();
            npc.Id = NpcDB.Instance.GetFirstAvalibleId();
            NpcDB.Instance.Add(npc);
        }

        private static void RemoveNpc(int index)
        {
            EditorUtility.SetDirty(NpcDB.Instance);
            NpcDB.Instance.RemoveAt(index);
        }

        private static void RemoveNpc(NPC npc)
        {
            EditorUtility.SetDirty(NpcDB.Instance);
            NpcDB.Instance.Remove(npc);
        }
}
