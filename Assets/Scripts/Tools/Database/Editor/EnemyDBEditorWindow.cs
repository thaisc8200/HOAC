﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class EnemyDBEditorWindow 
{
    private static Vector2 m_Scroll;
        private static EnemySO m_HoveredEnemy;

        public static void DrawWindow()
        {
            Controls();

            EditorGUILayout.BeginVertical(GUI.skin.box, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("EnemyDB");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            m_Scroll = EditorGUILayout.BeginScrollView(m_Scroll, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

            DrawHeaders();

            Event e = Event.current;
            if (e.type != EventType.Layout)
            {
                m_HoveredEnemy = null;
            }

            if (EnemyDB.Instance.RowCount > 0)
            {
                for (int i = 0; i < EnemyDB.Instance.Count; i++)
                {
                    DrawEnemy(i, EnemyDB.Instance.GetAtIndex(i));
                }
            }

            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();

            if (GUI.changed)
            {
                EditorUtility.SetDirty(EnemyDB.Instance);
            }
        }

        private static void Controls()
        {
            Event e = Event.current;
            switch (e.type)
            {
                case EventType.MouseUp:
                    if (e.button == 0)
                    {
                        if (m_HoveredEnemy != null)
                        {
                            Selection.activeObject = m_HoveredEnemy;
                            EditorGUIUtility.PingObject(m_HoveredEnemy);
                        }
                    }
                    if (e.button == 1)
                    {
                        GenericMenu menu = new GenericMenu();
                        if (m_HoveredEnemy != null)
                        {
                            menu.AddItem(new GUIContent("Remove"), false, () => RemoveEnemy(m_HoveredEnemy));
                        }
                        menu.DropDown(new Rect(Event.current.mousePosition, Vector2.zero));
                    }
                    break;
            }
        }

        private static void DrawHeaders()
        {
            GUILayout.BeginHorizontal("box");

            GUILayout.BeginHorizontal("box", GUILayout.Width(150));
            GUILayout.Label("ID");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Width(250));
            GUILayout.Label("Name");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Width(300));
            GUILayout.Label("Stats");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.MinWidth(300));
            GUILayout.Label("Description");
            GUILayout.EndHorizontal();

            GUILayout.EndHorizontal();
        }

        private static void DrawEnemy(int i, EnemySO enemy)
        {
            SerializedObject target = new SerializedObject(enemy);

            EditorGUIUtility.labelWidth = 75;

            GUILayout.BeginHorizontal("box", GUILayout.Height(75));

            // ID
            GUILayout.BeginVertical("box", GUILayout.Width(150), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(target.FindProperty("ID"));
            GUILayout.EndVertical();

            // Name
            GUILayout.BeginVertical("box", GUILayout.Width(250), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            GUILayout.Label(enemy.name, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false));
            GUILayout.EndVertical();

            // Stats
            EditorGUIUtility.labelWidth = 150;

            GUILayout.BeginVertical("box", GUILayout.Width(300), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(target.FindProperty("Stats"), true);
            GUILayout.EndVertical();

            EditorGUIUtility.labelWidth = 75;

            GUILayout.EndHorizontal();

            Event e = Event.current;
            Vector2 mousePos = Event.current.mousePosition;


            if (e.type != EventType.Layout)
            {
                Rect windowRect = new Rect(GUILayoutUtility.GetLastRect().position, GUILayoutUtility.GetLastRect().size);
                if (windowRect.Contains(mousePos)) m_HoveredEnemy = enemy;
            }

            target.ApplyModifiedProperties();
        }

        private static void RemoveEnemy(int index)
        {
            EditorUtility.SetDirty(EnemyDB.Instance);
            EnemyDB.Instance.RemoveAt(index);
        }

        private static void RemoveEnemy(EnemySO enemy)
        {
            EditorUtility.SetDirty(EnemyDB.Instance);
            EnemyDB.Instance.Remove(enemy);
        }

        private static void LabelProperty(string name, string value)
        {
            GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(name, GUILayout.Width(100));
            EditorGUILayout.LabelField(value, EditorStyles.helpBox, GUILayout.Width(40), GUILayout.ExpandWidth(true));
            GUILayout.EndHorizontal();
        }
}
