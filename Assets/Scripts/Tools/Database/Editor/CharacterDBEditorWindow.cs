﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class CharacterDBEditorWindow 
{
    private static Vector2 m_Scoll;
        private static CharacterSO m_HoveredCharacter;

        public static void DrawWindow()
        {
            Controls();

            EditorGUILayout.BeginVertical(GUI.skin.box, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("CharacterDB");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            m_Scoll = EditorGUILayout.BeginScrollView(m_Scoll, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

            DrawHeaders();

            Event e = Event.current;
            if (e.type != EventType.Layout)
            {
                m_HoveredCharacter = null;
            }

            if (CharacterDB.Instance.RowCount > 0)
            {
                for (int i = 0; i < CharacterDB.Instance.Count; i++)
                {
                    DrawCharacter(i, CharacterDB.Instance.GetAtIndex(i));
                }
            }

            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();

            if (GUI.changed)
            {
                EditorUtility.SetDirty(CharacterDB.Instance);
            }
        }

        private static void Controls()
        {
            Event e = Event.current;
            switch (e.type)
            {
                case EventType.MouseUp:
                    if (e.button == 0)
                    {
                        if (m_HoveredCharacter != null)
                        {
                            Selection.activeObject = m_HoveredCharacter;
                            EditorGUIUtility.PingObject(m_HoveredCharacter);
                        }
                    }
                    if (e.button == 1)
                    {
                        GenericMenu menu = new GenericMenu();
                        if (m_HoveredCharacter != null)
                        {
                            menu.AddItem(new GUIContent("Remove"), false, () => RemoveCharacter(m_HoveredCharacter));
                        }
                        menu.AddItem(new GUIContent("Add"), false, () => AddNewCharacter());
                        menu.DropDown(new Rect(Event.current.mousePosition, Vector2.zero));
                    }
                    break;
            }
        }

        private static void DrawHeaders()
        {
            GUILayout.BeginHorizontal("box");

            GUILayout.BeginHorizontal("box", GUILayout.Width(150));
            GUILayout.Label("ID");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Width(250));
            GUILayout.Label("Name");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Width(300));
            GUILayout.Label("Stats");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Width(300));
            GUILayout.Label("Stats Progression");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Width(200));
            GUILayout.Label("Prefab");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.MinWidth(300));
            GUILayout.Label("Sprite");
            GUILayout.EndHorizontal();

            GUILayout.EndHorizontal();
        }

        private static void DrawCharacter(int i, CharacterSO character)
        {
            SerializedObject target = new SerializedObject(character);

            EditorGUIUtility.labelWidth = 75;

            GUILayout.BeginHorizontal("box", GUILayout.Height(75));

            // ID
            GUILayout.BeginVertical("box", GUILayout.Width(150), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(target.FindProperty("ID"));
            GUILayout.EndVertical();

            // Name
            GUILayout.BeginVertical("box", GUILayout.Width(250), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            GUILayout.Label(character.name, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(false));
            GUILayout.EndVertical();

            // Stats
            GUILayout.BeginVertical("box", GUILayout.Width(300), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(target.FindProperty("Stats"), true);
            GUILayout.EndVertical();

            EditorGUIUtility.labelWidth = 200;

            // Stats Progression
            GUILayout.BeginVertical("box", GUILayout.Width(300), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(target.FindProperty("StatsProgression"), true);
            GUILayout.EndVertical();

            EditorGUIUtility.labelWidth = 75;

            // Prefab
            GUILayout.BeginHorizontal("box", GUILayout.Width(200), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            var objectField = AssetDatabase.LoadAssetAtPath(character.GamePrefab, typeof(Character));
            character.GamePrefab = AssetDatabase.GetAssetPath(EditorGUILayout.ObjectField(objectField, typeof(Character), true));
            GUILayout.EndHorizontal();

            // GUI
            GUILayout.BeginVertical("box", GUILayout.Width(300), GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(target.FindProperty("Portrait"), GUILayout.MinWidth(100));
            EditorGUILayout.Space();
            GUILayout.EndVertical();

            GUILayout.EndHorizontal();

            Event e = Event.current;
            Vector2 mousePos = Event.current.mousePosition;


            if (e.type != EventType.Layout)
            {
                Rect windowRect = new Rect(GUILayoutUtility.GetLastRect().position, GUILayoutUtility.GetLastRect().size);
                if (windowRect.Contains(mousePos)) m_HoveredCharacter = character;
            }

            target.ApplyModifiedProperties();
        }

        private static void AddNewCharacter()
        {
            EditorUtility.SetDirty(CharacterDB.Instance);
            CharacterSO character = ScriptableObject.CreateInstance<CharacterSO>();
            character.Id = CharacterDB.Instance.GetFirstAvalibleId();
            CharacterDB.Instance.Add(character);
        }

        private static void RemoveCharacter(int index)
        {
            EditorUtility.SetDirty(CharacterDB.Instance);
            CharacterDB.Instance.RemoveAt(index);
        }

        private static void RemoveCharacter(CharacterSO character)
        {
            EditorUtility.SetDirty(CharacterDB.Instance);
            CharacterDB.Instance.Remove(character);
        }
}
