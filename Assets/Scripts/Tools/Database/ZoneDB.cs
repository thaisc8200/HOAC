﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "ZoneDB", menuName = "G1/Database/ZoneDB")]
public class ZoneDB : AbstractDatabase<Zone>
{
    private static ZoneDB m_Instance = null;

    public static ZoneDB Instance
    {
        get
        {
            if (m_Instance == null)
            {
                LoadDatabase();
            }

            return m_Instance;
        }
    }

    protected override void OnAddObject(Zone t)
    {
#if UNITY_EDITOR
        t.name = "ZONE" + t.Id;
        AssetDatabase.AddObjectToAsset(t, this);
        AssetDatabase.SaveAssets();
        EditorUtility.SetDirty(this);
#endif
    }

    protected override void OnRemoveObject(Zone t)
    {
#if UNITY_EDITOR
        AssetDatabase.RemoveObjectFromAsset(t);
        AssetDatabase.SaveAssets();
        EditorUtility.SetDirty(this);
#endif
    }

    public static void LoadDatabase()
    {
#if UNITY_EDITOR
        m_Instance = (ZoneDB)AssetDatabase.LoadAssetAtPath("Assets/ScriptableObjects/Model/ZoneDB.asset", typeof(ZoneDB));
#else
            AssetBundle bundle = MyAssetBundle.LoadAssetBundleFromFile("g1/environment/zones");
            m_Instance = bundle.LoadAsset<ZoneDB>("ZoneDB");
            bundle.Unload(false);
#endif
    }

    public static void ClearDatabase()
    {
        m_Instance = null;
    }
}
