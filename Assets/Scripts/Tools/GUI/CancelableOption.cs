﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class CancelableOption : SelectableOption, ICancelHandler, IPointerExitHandler, IPointerEnterHandler
{
    public UnityEvent onCancel;

    public void OnCancel(BaseEventData eventData)
    {
        onCancel.Invoke();
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
        EventSystem.current = null;
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        EventSystem.current.SetSelectedGameObject(gameObject);
    }
}
