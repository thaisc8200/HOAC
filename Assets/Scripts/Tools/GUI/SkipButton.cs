﻿using System;
using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;
using UnityEngine.Events;

public class SkipButton : MonoBehaviour
{
    public UnityEvent OnSkipClicked;

    private void Update()
    {
        if (GetButtonDown(ReInput.players, "Skip"))
        {
            OnSkipClicked.Invoke();
        }
    }
    
    public bool GetButtonDown(ReInput.PlayerHelper players, string button)
    {
        foreach (var player in players.GetPlayers())
        {
            if (player.GetButtonDown(button))
            {
                return true;
            }
        }
        return false;
    }
}
